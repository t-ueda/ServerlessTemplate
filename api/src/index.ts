import {Callback, Context} from "aws-lambda";
export function hello(event, context: Context, callback: Callback): void {
    callback(undefined, {
        statusCode: 200,
        headers: {},
        body: JSON.stringify({message: "Hello AWS Lambda"}),
    });
}
